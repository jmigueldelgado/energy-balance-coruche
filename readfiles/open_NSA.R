#install_github("jmigueldelgado/micrometeo")
require(micrometeo)
require(dplyr)
require(lubridate)

fpath <- "./field_data/understory/"

f <-   list.files(fpath,pattern="^NSA[.]")

ll <- lapply(paste0(fpath,f),read_COMBILOG)

df <- bind_rows(ll)

## remove duplicates by datetime
tbl.nsa <- df %>% arrange(datetime) %>%
    distinct(datetime,.keep_all=TRUE) %>%
    select(matches("^[^.]*$"))


