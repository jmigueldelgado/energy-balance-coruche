# Energy balance for the understory in corkoak forest in Coruche, Portugal

This file contains basic info on the project and logs of field visits starting in 2018. Scroll down for going back in time.

## Field Visit 10 March 2018

- did not download any data
- packed clearing station number one and moved it to Carvalhais
- left behind the mast, stored inside the eddy tower precinct
- packed all batteries (one large and two small original batteries and the one bought in  Lisboa later in 2014) that were remaining in Coruche and moved them to Carvalhais.
- moved all but one solar panel (a small one) to Carvalhais.
